# Maintainer: Alex McGrath <amk@amk.ie>
pkgname=foot
pkgver=1.15.0
pkgrel=0
pkgdesc="Fast, lightweight and minimalistic Wayland terminal emulator"
url="https://codeberg.org/dnkl/foot"
license="MIT"
arch="all"
depends="ncurses-terminfo"
makedepends="
	cage
	font-dejavu
	fcft-dev
	fontconfig-dev
	freetype-dev
	libxkbcommon-dev
	meson
	ncurses
	pixman-dev
	scdoc
	tllist-dev
	utf8proc-dev
	wayland-dev
	wayland-protocols
	"
subpackages="
	$pkgname-dbg
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	$pkgname-extra-terminfo:_extra_terminfo:noarch
	$pkgname-themes:_themes:noarch
	"
source="
	$pkgname-$pkgver.tar.gz::https://codeberg.org/dnkl/foot/archive/$pkgver.tar.gz
	"
options="!check" # ran during profiling
builddir="$srcdir/foot"

build() {
	export CFLAGS="$CFLAGS -O3" # -O3 as the package is intended to use it
	export CXXFLAGS="$CXXFLAGS -O3"
	export CPPFLAGS="$CPPFLAGS -O3"

	abuild-meson \
		-Db_pgo=generate \
		-Db_lto=true \
		-Dutmp-backend=none \
		. output
	meson compile -C output

	ninja -C output test
	./pgo/full-headless-cage.sh . output

	meson configure -Db_pgo=use output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

_extra_terminfo() {
	pkgdesc="$pkgdesc (extra terminfo data)"

	install -d "$subpkgdir"/usr/share/terminfo/f
	mv "$pkgdir"/usr/share/terminfo/f/foot "$subpkgdir"/usr/share/terminfo/f/foot-extra
	mv "$pkgdir"/usr/share/terminfo/f/foot-direct "$subpkgdir"/usr/share/terminfo/f/foot-extra-direct
}

_themes() {
	pkgdesc="$pkgdesc (color schemes)"

	amove /usr/share/foot/themes
}

sha512sums="
b0d4ad025010e2dbe30e1cfbc5d41c7dabc2d2d99872193134ddbca043d0d63daf85f2e81bcfa4aabcf7335c38e952bb1e46a9d28a1cd8ac62fc14c6bf87cb20  foot-1.15.0.tar.gz
"
