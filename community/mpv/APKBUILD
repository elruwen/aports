# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: Jakub Skrzypnik <j.skrzypnik@openmailbox.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mpv
# intentionally git, see
# https://github.com/mpv-player/mpv#release-cycle
# the stable releases are just arbitrary git, so we can update more often.
# it's the same stability policy regardless
pkgver=0.35.1_git20230716
pkgrel=1
_gitrev=cab544889a6ce2dd48a5859b54ccfd807518bd92
pkgdesc="Video player based on MPlayer/mplayer2"
url="https://mpv.io/"
license="GPL-2.0-or-later"
arch="all"
makedepends="
	alsa-lib-dev
	ffmpeg-dev
	jack-dev
	libao-dev
	libarchive-dev
	libass-dev
	libbluray-dev
	libcdio-paranoia-dev
	libdvdnav-dev
	libplacebo-dev
	libva-dev
	libxext-dev
	libxinerama-dev
	libxkbcommon-dev
	libxpresent-dev
	libxrandr-dev
	libxscrnsaver-dev
	libxv-dev
	mesa-dev
	meson
	pipewire-dev
	pulseaudio-dev
	py3-docutils
	rubberband-dev
	shaderc-dev
	sndio-dev
	uchardet-dev
	vulkan-headers
	vulkan-loader-dev
	wayland-dev
	wayland-protocols
	zimg-dev
	zlib-dev
	"
subpackages="
	$pkgname-dbg
	$pkgname-doc
	$pkgname-libs
	$pkgname-dev
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
source="https://github.com/mpv-player/mpv/archive/$_gitrev/mpv-$_gitrev.tar.gz"
builddir="$srcdir/mpv-$_gitrev"
options="!check" # tests are for development

case "$CARCH" in
ppc64le|riscv64)
	makedepends="$makedepends lua5.2-dev"
	;;
*)
	makedepends="$makedepends luajit-dev"
	;;
esac

# secfixes:
#   0.27.0-r3:
#     - CVE-2018-6360

prepare() {
	default_prepare

	echo "${pkgver%_git*}-$_gitrev" > VERSION
}

build() {
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Dalsa=enabled \
		-Dbuild-date=false \
		-Dcdda=enabled \
		-Ddvdnav=enabled \
		-Degl-drm=enabled \
		-Degl-wayland=enabled \
		-Degl-x11=enabled \
		-Degl=enabled \
		-Dgl=enabled \
		-Djack=enabled \
		-Dlibplacebo=enabled \
		-Dlibplacebo-next=enabled \
		-Dlibmpv=true \
		-Dpulse=enabled \
		-Dsndio=enabled \
		-Duchardet=enabled \
		-Dvdpau=disabled \
		-Dvulkan=enabled \
		-Dx11=enabled \
		. output

	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	install -Dm644 "$builddir"/DOCS/*.rst "$builddir"/DOCS/*.md \
		-t "$pkgdir"/usr/share/doc/$pkgname/

	# Move example configuration files to subdirectory
	mkdir -p "$pkgdir"/usr/share/doc/mpv/examples
	mv "$pkgdir"/usr/share/doc/mpv/*.conf \
		"$pkgdir"/usr/share/doc/mpv/examples/
}

sha512sums="
03cfbf4dbeb14ae4c0e43df9c0fc902ec6f6933b9e71457a8581f090d1dc9e6c957dadf321f1b8f094da5cbda165ed657fad7d44bc391662d6b101777ac05c62  mpv-cab544889a6ce2dd48a5859b54ccfd807518bd92.tar.gz
"
