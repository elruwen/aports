# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=cargo-c
pkgver=0.9.21
pkgrel=0
pkgdesc="cargo subcommand to build and install C-ABI compatibile dynamic and static libraries"
url="https://github.com/lu-zero/cargo-c"
arch="all"
license="MIT"
# nghttp2-sys doesn't support system
makedepends="
	cargo
	cargo-auditable
	curl-dev
	libgit2-dev
	libssh2-dev
	openssl-dev>3
	zlib-dev
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/lu-zero/cargo-c/archive/v$pkgver.tar.gz
	$pkgname-$pkgver-Cargo.lock::https://github.com/lu-zero/cargo-c/releases/download/v$pkgver/Cargo.lock"
options="net" # To download crates

export LIBSSH2_SYS_USE_PKG_CONFIG=1
export DEP_NGHTTP2_ROOT=/usr

prepare() {
	default_prepare

	cp "$srcdir"/$pkgname-$pkgver-Cargo.lock Cargo.lock
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	rm target/release/cargo-*.d
	install -Dm755 target/release/cargo-* -t "$pkgdir"/usr/bin/

	install -Dm644 -t "$pkgdir/usr/share/doc/cargo-c" README.md
}

sha512sums="
855391c29843f8e5f204f889cab16d5d569ebb2174367a8be0d4be3d87141133f98fb7a6750ed0030783a1ff29f358d60c6ca79ed5bb65f61196c726f9c1a0ec  cargo-c-0.9.21.tar.gz
dd2b35e63991c44636583f01402eb5cabd20c38b1a6fb5c01baa5ccf803b099574aff7be7557abe5fb603448b19bc2faf8ad88a2931f34f8d21fc2eba53d58b2  cargo-c-0.9.21-Cargo.lock
"
