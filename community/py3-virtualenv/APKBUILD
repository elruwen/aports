# Contributor: Sam Dodrill <shadowh511@gmail.com>
# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: Sam Dodrill <shadowh511@gmail.com>
pkgname=py3-virtualenv
pkgver=20.24.0
pkgrel=0
pkgdesc="Virtual Python3 Environment builder"
options="!check" # Requires unpackaged 'flaky'
url="https://virtualenv.pypa.io/en/latest/"
arch="noarch"
license="MIT"
depends="py3-platformdirs py3-distlib py3-filelock"
makedepends="
	py3-gpep517
	py3-hatchling
	py3-hatch-vcs
	py3-wheel
	"
checkdepends="py3-pytest py3-six"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/v/virtualenv/virtualenv-$pkgver.tar.gz"
builddir="$srcdir/virtualenv-$pkgver"

replaces="py-virtualenv" # Backwards compatibility
provides="py-virtualenv=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 setup.py test
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
7c3974ebd40d855bb859e3af10e341c6e8164d63779d6038bac15d49fdb1b87c564c6f7987d06d2f01fb4009183dfe4e05fa2dadaad44b8a153b7c1cb55b687e  virtualenv-20.24.0.tar.gz
"
